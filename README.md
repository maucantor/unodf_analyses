# The UNODF_analyses repository 
------------------------------------------------

#### Author, maintainer and contact
Mauricio Cantor

Department of Biology, Dalhousie University, 1355 Oxford Street, Halifax, NS B3H 4J1, Canada. 

Current address (2016): Departamento de Ecologia e Zoologia, Universidade Federal de Santa Catarina, Caixa Postal 5102, CEP 88040-970, Florianopolis, SC, Brazil. 

Email: m.cantor@ymail.com 

Telephone: +1 (902) 703-0915

#### Description
This repository contains all R scripts to run the analyses and create the figures of the manuscript cited below. It also contains the 18 data sets used in the manuscript. If you use any of them, please cite the original reference for the data set as described in the [READ-ME_metadata.txt](https://bitbucket.org/maucantor/unodf_analyses/src/7a1730d67902219b4309063104ef6e0d490deafb/data/_READ-ME_metadata.txt?at=master&fileviewer=file-view-default) file.

These scripts require the R package {UNODF} available at the [UNODF repository](https://bitbucket.org/maucantor/unodf)


#### Reference
[Cantor M, Pires MM, Marquitti FDM, Raimundo RLG, Sebastian-Gonzalez E, Coltri P, Perez I, Barneche D, Brandt DYC, Nunes K, Daura-Jorge FG, Floeter SR, Guimaraes PR Jr. 2017. Nestedness across biological scales. PLoS ONE 12(2): e0171691. doi:10.1371/journal.pone.0171691](http://journals.plos.org/plosone/article?id=10.1371/journal.pone.0171691)